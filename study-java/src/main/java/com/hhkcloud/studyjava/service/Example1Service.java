package com.hhkcloud.studyjava.service;

import com.hhkcloud.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     * 샘플 데이터를 생성한다.
     *
     * @return  Example1Item 리스트
     */
    private List<Example1Item> settingData() { // Example1Item 메소드를 List 형식으로 List 라는 한뭉치로
        List<Example1Item> result = new LinkedList<>(); // Example1Item의 리스트 생성

        Example1Item item1 = new Example1Item(); // 빈 item1에 아이템 생성
        item1.setName("홍길동"); // item1이라는 빈 공간에 setter의 setName "홍길동" 넣는다
        item1.setBirthday(LocalDate.of(2022,1,1)); //item1이라는 빈 공간에 setBirthday LocalDate형식의 값을 넣는다
        result.add(item1); // item1의 결과들을 add 집어 넣는다 result 결과에

        Example1Item item2 = new Example1Item(); // 빈 item2에 아이템 생성
        item2.setName("허형구"); // item2에 허형구 생성
        item2.setBirthday(LocalDate.of(2022,2,2)); // item2 2022 2 2 생성
        result.add(item2); //item2를 result에 추가 result는 List

        Example1Item item3 = new Example1Item(); // 빈 item2에 아이템 생성
        item3.setName("김아무개"); // item3에 김아무개 넣는다
        item3.setBirthday(LocalDate.of(2022,3,3)); // item3 2022 3 3 넣는다
        result.add(item3); //item3을 Example1Item 리스트에 넣는다

        Example1Item item4 = new Example1Item();
        item4.setName("박아무개"); // item4에 김아무개 넣는다
        item4.setBirthday(LocalDate.of(2022,4,4)); // item3 2022 4 4 넣는다
        result.add(item4); //item4을 Example1Item 리스트에 넣는다

        Example1Item item5 = new Example1Item();
        item5.setName("황아무개"); // item4에 황아무개 넣는다
        item5.setBirthday(LocalDate.of(2022,5,5)); // item3 2022 4 4 넣는다
        result.add(item5); //item5을 Example1Item 리스트에 넣는다

        return result; //result를 값을 밖으로 반환한다
    }

    /**
     *
     *
     * @return
     */
    public List<String> getForEachTest() { //공개적으로 사용 문자열형식의List 사용 이름은 get
        List<Example1Item> data = settingData(); // settingData가 List Example1Item 형식의 data 넣는다
        List<String> result = new LinkedList<>(); // 빈 LinkedList(링크드체인)의 공간을 문자열 형식인 result 변수에 생성

        data.forEach(item -> result.add(item.getName())); // data값을 한번씩 받아올거다 item.getName 하나씩 result 값에

        for (Example1Item item : data) { // List<Example1Item> 형식의 data값을 item 변수로 지정
            result.add((item.getName())); // result에 item안의 getName이라는 문자열데이터를 지정한다
        }

        return result; //리설트값을 밖으로 반환한다

        // 예상결과물 :
    }

}
