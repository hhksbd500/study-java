package com.hhkcloud.studyjava.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.core.SpringVersion;

import java.time.LocalDate;

@Getter //박스 name birtday라는 금을 숨기고있음 getter 금을 가저다주는사람 setter 금을넣어주는 사람
@Setter
public class Example1Item {
    private String name;
    private LocalDate birthday;
}
